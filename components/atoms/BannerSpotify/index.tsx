
const BannerSpotify = () => {
  return (
    <picture className="m-8">
        <img className="w-auto h-9" src="/Spotify_Logo_RGB_White.png" alt="" />
    </picture>
  )
}

export default BannerSpotify