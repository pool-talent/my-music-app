import React from 'react'
import { millisToMinutesAndSeconds } from '../../../services/lib/time';

interface Props {
    order: number;
    track: any;
}

export const Track = ({order, track}: Props) => {

  return (
    <div className="grid grid-cols-2 text-gray-500 py-4 px-5 hover:bg-gray-900 rounded-lg cursor-pointer" onClick={() => {/*playSong()*/}}>
        <div className="flex item-center space-x-4">
            <p>{order + 1}</p>
            <img className="h-10 w-10" src={(track.track.album.images.length > 0) ? track.track.album.images[0].url : '/default_playlist.jpg'} alt="" />
            <div>
                <p className="w-36 lg:w-64 text-white truncate">{track.track.name}</p>
                <p className="w-40">{track.track.artists[0].name}</p>
            </div>
        </div>

        <div className="flex items-center justify-between ml-auto md:ml-0">
            <p className="w-40 hidden md:inline">{track.track.album.name}</p>
            <p>{millisToMinutesAndSeconds(track.track.duration_ms)}</p>
        </div>
    </div>
  )
}
