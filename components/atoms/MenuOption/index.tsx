interface Props {
    label: string;
    Icon: any;
}

const MenuOption = ({label, Icon}: Props) => {
  return (
    <button className='flex items-center space-x-2 hover:text-white'>
        <Icon className='h-5 w-5' />
        <p>{label}</p>
    </button>
  )
}

export default MenuOption;