import { useSession } from 'next-auth/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getPlaylitsData } from '../../../store/actions/playlistsAction';
import { setContentPrincipalData } from '../../../store/actions/contentPrincipalAction';

const Playlists = () => {
    const { data: session } = useSession();
    const dispatch = useDispatch();
    const playlistsData = useSelector((state: any) => state.playlistsData.playlists);

    const selectPlaylist = (playlist: any) => {
        const content = {type: playlist.type, id: playlist.id, name: playlist.name, image: playlist.images[0]?.url};
        dispatch(setContentPrincipalData(content));
        
    }

    useEffect(() => {
        const playlistsLocalStorage = localStorage.getItem('PLAYLISTS');
        if (session?.expires && Date.now().toString() < session?.expires ) {
            dispatch(getPlaylitsData());
        }
    }, [dispatch, session])

    return (
        <>
            {/* Custom playlists */}
            {   
                playlistsData && playlistsData.length > 0 &&
                playlistsData.map((playlist: any) => (
                    <p key={playlist.id} onClick={() => {selectPlaylist(playlist)}} className='cursor-pointer hover:text-white'>{playlist.name}</p>
                ))
            }
            
        </>
    )
}

export default Playlists;