import {
    HomeIcon,
    SearchIcon,
    LibraryIcon,
    PlusCircleIcon,
    RssIcon,
    HeartIcon
} from '@heroicons/react/outline';
import MenuOption from '../../atoms/MenuOption';

const OPTIONS = [
    {
        label: "Home",
        icon: HomeIcon
    },
    {
        label: "Search",
        icon: SearchIcon
    },
    {
        label: "Your Library",
        icon: LibraryIcon
    },
    {
        label: "Create PLaylist",
        icon: PlusCircleIcon
    },
    {
        label: "Liked Playlist",
        icon: HeartIcon
    },
    {
        label: "Your Episodes",
        icon: RssIcon
    }
];

const MenuOptions = () => {
  return (
      <>
        {
            OPTIONS.map(({label, icon}) => (
                <MenuOption key={label} label={label} Icon={icon} />
            ))
        }
      </>
  )
}

export default MenuOptions;