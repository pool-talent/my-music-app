import { useSession } from 'next-auth/react';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Api from '../../../services/axios/api';
import { getTracksData } from '../../../store/actions/tracksAction';
import { Track } from '../../atoms/Track';

interface Props {
    idPLaylist: string;

}

const Tracks = ({idPLaylist}: Props) => {
    const { data: session } = useSession();
    const dispatch = useDispatch();
    const estadoGlobal = useSelector((state: any) => state);
    const tracksData = useSelector((state: any) => state.tracksData.tracks);
    const contentPrincipalData = useSelector((state: any) => state.contentPrincipalData.content);
    const [tracks, setTracks] = useState<any>([]);

    useEffect(() => {
        if (session?.expires && Date.now().toString() < session?.expires) {
            dispatch(getTracksData(idPLaylist));
            
        }
      }, [dispatch, session, contentPrincipalData])

    useEffect(() => {
        setTracks(tracksData.filter((track: any) => track.id === idPLaylist));
    }, [tracksData])
    

    return (
        <div>
            {
                
                tracksData && tracksData.length > 0 && tracks.length > 0 && tracks[0].tracks && tracks[0].tracks.length > 0 ?
                tracks[0].tracks.map((track: any, i: number) => (

                    <Track key={track.track.id} track={track} order={i} />
                ))
                : <div>Esta lista esta vacia.</div>
            }
        </div>
    )
}

export default Tracks