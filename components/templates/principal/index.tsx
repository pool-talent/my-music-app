import { ChevronDownIcon } from "@heroicons/react/outline";
import { signOut, useSession } from "next-auth/react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Tracks from "../../molecules/Tracks";

const Principal = () => {
  const { data: session } = useSession();
  const dispatch = useDispatch();
  const contentPrincipalData = useSelector((state: any) => state.contentPrincipalData.content);
  const userData = useSelector((state: any) => state.userData.user);
  
  useEffect(() => {
      
  }, [dispatch, session, contentPrincipalData, userData])

  return (
    <div className="flex-grow overflow-y-scroll h-[calc(100vh_-_17vh)]">
        <section className="flex">
            <div className="absolute top-5 right-8">
                <div className="flex items-center bg-black space-x-3 opacity-90 hover:opacity-80 cursor-pointer rounded-full p-1 pr-2 text-white" onClick={() =>signOut()}>
                    <img className="rounded-full w-7 h-7" src={userData.image} alt="" />
                    <span className="font-bold">{userData.name}</span>
                    <ChevronDownIcon className="h-5 w-5" />
                </div>
            </div>
        </section>
        {
            contentPrincipalData && contentPrincipalData.type && contentPrincipalData.type === "playlist" &&
            <>
                <section className={`flex items-end space-x-7 bg-gradient-to-b to-black from-green-500 h-80  text-white p-8`}>
                {/* <section className={`flex items-end space-x-7 bg-gradient-to-b to-black ${color} h-80  text-white p-8`}> */}
                    <img className="h-44 w-44 shadow-2xl" src={(contentPrincipalData.image) ? contentPrincipalData.image : '/default_playlist.jpg'} alt="playlist image" />
                    <div>
                        <p>Playlist</p>
                        {/* <h1 className="text-2xl md_text-3xl xl:text-5xl font-bold">{playlist?.name}</h1> */}
                        <h1 className="text-2xl md_text-3xl xl:text-5xl font-bold">{contentPrincipalData.name}</h1>
                    </div>
                </section>

                <div className="text-white">
                    <Tracks idPLaylist={contentPrincipalData.id} />
                </div>
            </>
        }
    </div>
  )
}

export default Principal;