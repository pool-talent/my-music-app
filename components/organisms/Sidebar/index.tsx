import { useSession } from "next-auth/react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getUserData } from '../../../store/actions/userAction';
import BannerSpotify from "../../atoms/BannerSpotify";
import MenuOptions from "../../molecules/MenuOptions";
import Playlists from "../../molecules/Playlists";

const Sidebar = () => {
    const { data: session } = useSession();
    const dispatch = useDispatch();
    

    useEffect(() => {
        if (session?.expires && Date.now().toString() < session?.expires) {
            dispatch(getUserData())
        }
    }, [dispatch, session])
    

    return (
        <div className='text-gray-500 p-5 text-xs lg:text-sm border-r border-gray-900 overflow-y-scroll h-[calc(100vh_-_17vh)] sm:max-w-[12rem] lg:max-w-[15rem] hidden md:inline-flex pb-36'>
            <div className='space-y-4'>
                <BannerSpotify />
                <MenuOptions />
                <hr className='border-t-[0.1px] border-gray-900'/>
                <Playlists />
            </div>
        </div>
    )
}

export default Sidebar;