import axios from 'axios';
import NextAuth from 'next-auth';
import SpotifyProvider from 'next-auth/providers/spotify';
import querystring from 'querystring';
import { loginUrl } from '../../../services/axios/spotify';

const refreshToken = async (token) => {
    try {
        let newToken;
        let url = `https://accounts.spotify.com/api/token`;
        let config = {
            headers: {
                'Authorization': `Basic ${new Buffer(`${process.env.NEXT_PUBLIC_CLIENT_ID}:${process.env.NEXT_PUBLIC_CLIENT_SECRET}`).toString('base64')}`,
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        let form = {
            grant_type: 'refresh_token',
            refresh_token: token.refreshToken
        }

        await axios.post(url, querystring.stringify(form), config).then((data) => {
            newToken = data.data;
        }).catch((err) => console.log(err));

         return {
             ...token,
             accessToken: newToken.access_token,
             accessTokenExpires: Date.now + newToken.expires_in * 1000,
             refreshToken: newToken.refresh_token ?? token.refreshToken
         };
    } catch (error) {
        console.error(error)

        return {
            ...token,
            error: "RefreshTokenError"
        };
    }
}

export default NextAuth({
    providers: [
        SpotifyProvider({
            clientId: process.env.NEXT_PUBLIC_CLIENT_ID,
            clientSecret: process.env.NEXT_PUBLIC_CLIENT_SECRET,
            authorization: loginUrl
        })
    ],
    secret: process.env.JWT_SECRET,
    pages: {
        signIn: '/login'
    },
    callbacks: {
        async jwt({ token, account, user }){
            
            //initial signIn
            if (account && user) {

                return {
                    ...token,
                    accessToken: account.access_token,
                    refreshToken: account.refresh_token,
                    username: account.providerAccountId,
                    accessTokenExpires: account.expires_at * 1000
                };
            }

            //Return Previous Token if the access token has not expired yet
            if (Date.now() < token.accessTokenExpires) {
                console.log("existing access token is called");
                return token;
            }

            //Access token has expired, so we need to refresh it...
            console.log("Access token has expired, refreshing...");

            return await refreshToken(token);
        },

        async session({ session, token }) {
            session.user.accessToken = token.accessToken;
            session.user.refreshToken = token.refreshToken;
            session.user.username = token.username;

            return session;
        }
    }
})