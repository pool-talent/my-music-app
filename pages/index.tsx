import type { NextPage } from 'next'
import Head from 'next/head'
import { useDispatch, useSelector } from "react-redux";
import Player from '../components/organisms/Player'
import Sidebar from '../components/organisms/Sidebar'
import Principal from '../components/templates/principal'

const Home: NextPage = () => {  

  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>My Music App</title>
        <link rel="icon" type='image/png' href="/Spotify_Icon_RGB_Green.png" />
      </Head>
      <main className='flex'>
        <Sidebar />
        <Principal />
      </main>
      <footer className='sticky bottom-0'>
        <Player />
      </footer>
    </div>
  )
}

export default Home
