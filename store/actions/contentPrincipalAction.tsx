import Api from '../../services/axios/api';
import { SET_CONTENT_PRINCIPAL_DATA, CONTENT_PRINCIPAL_DATA_ERROR } from '../types';

interface Content {
    type: string;
    id?: string;
    name?: string;
    image?: string;
}

export const setContentPrincipalData: any = (content: Content) => async (dispatch: any) => {
    try {

        dispatch({
            type: SET_CONTENT_PRINCIPAL_DATA,
            payload: content
        })
    } catch (error) {
        dispatch({
            type: CONTENT_PRINCIPAL_DATA_ERROR,
            payload: "error in content principal information"
        })
    }
}