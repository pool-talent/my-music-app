import { getSession } from 'next-auth/react';
import Api from '../../services/axios/api';
import { SET_TRACKS, TRACKS_ERROR } from '../types';

export const getTracksData: any = (id: string) => async (dispatch: any) => {
    try {
        const session: any = await getSession();
        const tracksLocalStorage = localStorage.getItem('TRACKS');
        let tracks: any[] = [];

        if (tracksLocalStorage) {
            tracks = JSON.parse(tracksLocalStorage);
        }

        const existElement = tracks.filter((track: any) => track.id === id);

        if (session && existElement.length === 0) {
            console.log("seejecuta petición playlists");
            await Api.get(`/v1/playlists/${id}/tracks`).then(({data}) => {

                tracks.push({id, tracks: data.items});
                localStorage.setItem('TRACKS', JSON.stringify(tracks));

            }).catch(err => console.log(err))
        }

        dispatch({
            type: SET_TRACKS,
            payload: tracks
        })
    } catch (error) {
        dispatch({
            type: TRACKS_ERROR,
            payload: "error in tracks"
        })
    }
}