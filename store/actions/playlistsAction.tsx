import { getSession } from 'next-auth/react';
import Api from '../../services/axios/api';
import { SET_PLAYLISTS_DATA, PLAYLISTS_DATA_ERROR } from '../types';

export const getPlaylitsData: any = () => async (dispatch: any) => {
    try {
        const session: any = await getSession();
        const playlistsLocalStorage = localStorage.getItem('PLAYLISTS');
        let playlists: any;

        if (session && !playlistsLocalStorage) {
            const userId:any = session?.user;
            console.log("seejecuta petición playlists");
            await Api.get(`/v1/users/${userId.username}/playlists`).then(({data}) => {
                playlists = data.items;
                localStorage.setItem('PLAYLISTS', JSON.stringify(data.items));
            }).catch(err => console.log(err))
        } else if (session && playlistsLocalStorage) {
            playlists = JSON.parse(playlistsLocalStorage);
        }

        dispatch({
            type: SET_PLAYLISTS_DATA,
            payload: playlists
        })
    } catch (error) {
        dispatch({
            type: PLAYLISTS_DATA_ERROR,
            payload: "error in playlists"
        })
    }
}