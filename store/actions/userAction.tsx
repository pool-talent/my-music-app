import Api from '../../services/axios/api';
import { SET_USER_DATA, USER_DATA_ERROR } from '../types';

export const getUserData: any = () => async (dispatch: any) => {
    try {
        const userLocalStorage = localStorage.getItem('INFO_LOCAL');
        let user: any;

        if (!userLocalStorage) {
            await Api.get('/v1/me').then(({data}) => {
                console.log("se ejecuta petición informacion del usuario");
                user = {
                    name: data.display_name,
                    image: data.images[0].url
                }
                localStorage.setItem('INFO_LOCAL', JSON.stringify({name: data.display_name, image: data.images[0].url}));
            }).catch(err => console.log(err))
        } else {
            user = JSON.parse(userLocalStorage);
        }

        dispatch({
            type: SET_USER_DATA,
            payload: user
        })
    } catch (error) {
        dispatch({
            type: USER_DATA_ERROR,
            payload: "error in user information"
        })
    }
}