import { SET_PLAYLISTS_DATA, PLAYLISTS_DATA_ERROR } from '../types';

const initialState = {
    playlists: {}
}

const playlistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PLAYLISTS_DATA:
            return {
                ...state,
                playlists: action.payload,
            }

        case PLAYLISTS_DATA_ERROR:
            let error = action.payload
            return {
                ...state,
                playlists: {error},
            }

        default:
            return state;
    }
}

export default playlistsReducer;