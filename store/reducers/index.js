import { combineReducers } from "redux";
import tracksReducer from './tracksReducer';
import userReducer from './userReducer';
import playlistsReducer from './playlistsReducer';
import contentPrincipalReducer from './contentPrincipalReducer';

export default combineReducers({
    tracksData: tracksReducer,
    userData: userReducer,
    playlistsData: playlistsReducer,
    contentPrincipalData: contentPrincipalReducer
});