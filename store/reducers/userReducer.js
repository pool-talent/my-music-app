import { SET_USER_DATA, USER_DATA_ERROR } from '../types'

const initialState = {
    user: {
        name: '',
        image: ''
    }
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                user: action.payload,
            }

        case USER_DATA_ERROR:
            let error = action.payload
            return {
                ...state,
                user: {error},
            }

        default:
            return state;
    }
}

export default userReducer;