import { SET_CONTENT_PRINCIPAL_DATA, CONTENT_PRINCIPAL_DATA_ERROR } from '../types'

const initialState = {
    content: {}
}

const contentPrincipalReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CONTENT_PRINCIPAL_DATA:
            return {
                ...state,
                content: action.payload,
            }

        case CONTENT_PRINCIPAL_DATA_ERROR:
            let error = action.payload
            return {
                ...state,
                content: {error},
            }

        default:
            return state;
    }
}

export default contentPrincipalReducer;