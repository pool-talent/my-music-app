import { SET_TRACKS, TRACKS_ERROR } from '../types'

const initialState = {
    tracks: []
}

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_TRACKS:
            return {
                ...state,
                tracks: action.payload,
            }

        case TRACKS_ERROR:
            let error = action.payload
            return {
                ...state,
                tracks: {error},
            }

        default:
            return state;
    }
}

export default tracksReducer;