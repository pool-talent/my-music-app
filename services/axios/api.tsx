import axios from 'axios';
import { getSession } from 'next-auth/react';

const baseURL = 'https://api.spotify.com';

const Api = axios.create({baseURL})

Api.interceptors.request.use(
    async(config: any) => {
        const session: any = await getSession();
        const userToken:string = session.user.accessToken;

        if (userToken.length > 0) {
            config.headers['Authorization'] = `Bearer ${userToken}`;
        }
        config.headers['Content-Type'] = 'application/json'
        return config
    }
)

export default Api;
